#!/bin/sh
#Authored by @sirmrgentleman on gitlab.
echo $out
mkdir -p "$out/nano"
curl -L https://www.nano-editor.org/dist/latest/nano-7.2.tar.gz | tar -xz -C "$out/"
cd "$out/nano" && "$out/nano"*/configure
make DESTDIR="$rfs" install
cd "$out"
rm -rf "$out/nano"*
rm -rf "$out/nano"
