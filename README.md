# akoziland-linux-scripts

# About
An unofficial collection of scripts used in the configuration of an Akoziland Linux system. https://gitlab.com/akoziland
This includes configurations, programs, build scripts, and more. Submissions are welcome.
